# Simple Python Unit Tests Example

Examples:

1. test_calc and test_greeting test a python module
2. test_calculator tests a python object method
3. test_mock_return_value mocks the return value of a method
4. test_mock_side_effect creates a mock_method to test
5. test_mock_advanced mocks the module, then shows changing the return value in a specific test method


# Setup
```
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt
```

# Run Tests

```
py.test --cov=app --cov-report=html tests/
```


# References
* https://semaphoreci.com/community/tutorials/getting-started-with-mocking-in-python 