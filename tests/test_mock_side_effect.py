import unittest
from unittest.mock import patch

def mock_simple(name):
  # Does not have the sleep function
  return "Hello {name}".format(name=name)

class TddInPythonExample(unittest.TestCase):
  @patch('app.greeting.simple', side_effect=mock_simple)
 
  def test_simple_greeting(self, simple):
    self.assertEqual(simple("Bob"), "Hello Bob")

if __name__ == '__main__':
  unittest.main()