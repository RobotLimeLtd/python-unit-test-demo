import unittest
from unittest.mock import patch

class TddInPythonExample(unittest.TestCase):
  @patch('app.greeting.simple', return_value="Hello Bob") # Patch the module method
 
  def test_simple_greeting(self, simple):
    self.assertEqual(simple("Bob"), "Hello Bob")

if __name__ == '__main__':
  unittest.main()