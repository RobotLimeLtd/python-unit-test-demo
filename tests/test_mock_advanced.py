import unittest
from unittest.mock import patch

class TddInPythonExample(unittest.TestCase):
 
  @patch('app.greeting') # Patch the module
  
  def test_simple_greeting(self, mocked_greeting):
    mocked_greeting.simple.return_value = "Hello Bob" # Set the return value for this specific test
    self.assertEqual(mocked_greeting.simple("Bob"), "Hello Bob")
    mocked_greeting.simple.assert_called_once_with("Bob")

if __name__ == '__main__':
  unittest.main()