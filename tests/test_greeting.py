import unittest
from app.greeting import simple

class TddInPythonExample(unittest.TestCase):

  def test_simple_greeting(self):
    result = simple("Bob") # this includes the 5 second wait
    self.assertEqual("Hello Bob", result)

if __name__ == '__main__':
  unittest.main()