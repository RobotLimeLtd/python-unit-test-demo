import unittest
from app.calc import add

class TddInPythonExample(unittest.TestCase):

  def test_calculator_add(self):
    result = add(2,2)
    self.assertEqual(4, result)

  def test_calculator_returns_error_message_if_both_args_not_numbers(self):
    self.assertRaises(ValueError, add, 'two', 'three')


if __name__ == '__main__':
  unittest.main()