import time

def simple(name):
  return "Hello {name}".format(name=name)
